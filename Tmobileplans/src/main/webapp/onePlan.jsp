<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>T-Mobile OnePlan</title>
<style type="text/css">
td{
	font-size: 120%
}
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
</head>
<body bgcolor="#C1E1A6">
	<div align="center">
		<h1>T-Mobile OnePlan details</h1>
		<hr />
	</div>
	<div align="center">
		<table>
			<tr>
				<td>Free nextflix offer to customers subscribed to this plan</td>
			</tr>
			<tr>
				<td>Free unlimited internet provided</td>
			</tr>
			<tr>
				<td>$70 per 1 person, 57 per 2 persons,45 per 3 persons,40 per 4 person and additional $20 for each additional line</td>
			</tr>
			<tr>
				<td>Additional $5 per line if not subscribed for autopay</td>
			</tr>
		</table>
		<form action="LinesController" method="post">
			<button name="Proceed" value="Proceed" type="submit" class="button">Proceed</button>
			<button name="Back" value="Back" type="submit" class="button">Back</button>
			<input type="hidden" name="onePlan" value="onePlan">
		</form>
	</div>
</body>
</html>