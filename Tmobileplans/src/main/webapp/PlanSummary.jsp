<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>T-Mobile PlanSummary</title>
<style type="text/css">
td{
	font-size: 120%
}
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
</head>
<body bgcolor="#C1E1A6">
	<div align="center">
		<h1>T-Mobile Plan Summary</h1>
		<hr />
	</div>
	<div align="center">
		<table>
			<tr>
				<th><h2>
						Hello
						<c:out value="${detailsBean.lastName}"></c:out>
						<c:out value="${detailsBean.firstName}"></c:out>
						Your Plan Summary is below
					</h2></th>
			</tr>
		</table>
	</div>
	<c:if test="${planName eq 'seniorCitizenPlan'}">
		<div align="center">
			<table>
				<tr>
					<td>Plan Name :</td>
					<td><c:out value="${planName}"></c:out></td>
				</tr>
				<tr>
					<td>No of Connections :</td>
					<td><c:out value="${detailsBean.noOfLines}"></c:out></td>
				</tr>
				<tr>
					<td>Auto Pay :</td>
					<c:if test="${detailsBean.autoPay == 'false'}">
						<td>Not Subscribed</td>
					</c:if>
					<c:if test="${detailsBean.autoPay == 'true'}">
						<td>Subscribed</td>
					</c:if>
				</tr>
				<tr>
					<td colspan="2" align="center"><h3>
							Features of T-Mobile
							<c:out value="${planName}"></c:out></h3></td>
				</tr>
				<tr>
					<td>Free nextflix offer to customers subscribed to this plan</td>
				</tr>
				<tr>
					<td>Free unlimited internet provided</td>
				</tr>
				<tr>
					<td>$30 for each line</td>
				</tr>
				<tr>
					<td>Additional $5 per line if not subscribed for autopay</td>
				</tr>
				<tr>
					<td><h4>Total Cost of the plan is</h4></td>
					<td><h4>
							<c:out value="${detailsBean.totalPlanCost}"></c:out>
						</h4></td>
				</tr>
			</table>
		</div>
	</c:if>
	<c:if test="${planName eq 'onePlan'}">
		<div align="center">
			<table>
				<tr>
					<td>Plan Name :</td>
					<td><c:out value="${planName}"></c:out></td>
				</tr>
				<tr>
					<td>No of Connections :</td>
					<td><c:out value="${detailsBean.noOfLines}"></c:out></td>
				</tr>
				<tr>
					<td>Auto Pay :</td>
					<c:if test="${detailsBean.autoPay == 'false'}">
						<td>Not Subscribed</td>
					</c:if>
					<c:if test="${detailsBean.autoPay == 'true'}">
						<td>Subscribed</td>
					</c:if>
				</tr>
				<tr>
					<td colspan="2" align="center"><h3>
							Features of T-Mobile
							<c:out value="${planName}"></c:out></h3></td>
				</tr>
				<tr>
				<td>Free nextflix offer to customers subscribed to this plan</td>
			</tr>
			<tr>
				<td>Free unlimited internet provided</td>
			</tr>
			<tr>
				<td>$70 per 1 person, 57 per 2 persons,45 per 3 persons,40 per 4 person and additional $20 for each additional line</td>
			</tr>
			<tr>
				<td>Additional $5 per line if not subscribed for autopay</td>
			</tr>
				<tr>
					<td><h4>Total Cost of the plan is</h4></td>
					<td><h4>
							<c:out value="${detailsBean.totalPlanCost}"></c:out>
						</h4></td>
				</tr>
			</table>
		</div>
	</c:if>
	<c:if test="${planName eq 'familyOfTwoPlan'}">
		<div align="center">
			<table>
				<tr>
					<td>Plan Name :</td>
					<td><c:out value="${planName}"></c:out></td>
				</tr>
				<tr>
					<td>No of Connections :</td>
					<td><c:out value="${detailsBean.noOfLines}"></c:out></td>
				</tr>
				<tr>
					<td>Auto Pay :</td>
					<c:if test="${detailsBean.autoPay == 'false'}">
						<td>Not Subscribed</td>
					</c:if>
					<c:if test="${detailsBean.autoPay == 'true'}">
						<td>Subscribed</td>
					</c:if>
				</tr>
				<tr>
					<td colspan="2" align="center"><h3>
							Features of T-Mobile
							<c:out value="${planName}"></c:out></h3></td>
				</tr>
				<tr>
					<td>Free nextflix offer to customers subscribed to this plan</td>
				</tr>
				<tr>
					<td>Free unlimited internet provided</td>
				</tr>
				<tr>
					<td>$60 for each line</td>
				</tr>
				<tr>
					<td>Additional $5 per line if not subscribed for autopay</td>
				</tr>
				<tr>
					<td><h4>Total Cost of the plan is</h4></td>
					<td><h4>
							<c:out value="${detailsBean.totalPlanCost}"></c:out>
						</h4></td>
				</tr>
			</table>
		</div>
	</c:if>
	<div align="center">
		<a href="index.jsp">Home</a>
	</div>
</body>
</html>