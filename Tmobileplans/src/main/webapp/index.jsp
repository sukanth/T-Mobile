
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to T-Mobile</title>
<style type="text/css">
td {
	font-size: 120%
}

.button {
	background-color: #4CAF50;
	border: none;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
	cursor: pointer;
}
</style>
</head>
<body bgcolor="#C1E1A6">
	<form method="post" action="LinesController">
		<div align="center">
			<h1>Welcome to T-Mobile Plans</h1>
			<table>
				<tr>
					<td>Please select a plan</td>

					<td><select name="planCategory">
							<option value="TO">T-Mobile One Plan</option>
							<option value="TSC">T-Mobile Senior Citizen Plan</option>
							<option value="TFF">T-Mobile Family For Two Plan</option>
					</select></td>
				</tr>
			</table>
			<input type="submit" value="Submit" class="button" />
		</div>
	</form>
</body>
</html>