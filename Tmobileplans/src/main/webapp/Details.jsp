<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>T-Mobile Details</title>
<style type="text/css">
td {
	font-size: 120%
}

.button {
	background-color: #4CAF50;
	border: none;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
	cursor: pointer;
}
</style>
</head>
<body bgcolor="#C1E1A6">
	<div align="center">
		<h1>Please enter details below</h1>
		<hr />
	</div>
	<div align="center">
		<form action="CalculateCost" method="post">
			<table>
				<tr>
					<td>Enter first name :</td>
					<td><input type="text" name="firstName"></td>
				<tr>
				<tr>
					<td>Enter last name :</td>
					<td><input type="text" name="lastName"></td>
				<tr>
				<tr>
					<td>Social Security</td>
					<td><input type="text" name="socialSecurity"></td>
				<tr>
				<tr>
					<td>No of Connections :</td>
					<td><input type="text" name="lines"></td>
				<tr>
				<tr>
					<td>Age :</td>
					<td><input type="text" name="age"></td>
				<tr>
				<tr>
					<td>AutoPay :</td>
					<td><input type="checkbox" name="autoPay"></td>
				<tr>
			</table>
			<input type="submit" value="submit" class="button">
			<input type="hidden" name="planName" value="${planName}">
		</form>
	</div>
</body>
</html>