package capgemini;

public class TmobileOnePlan extends Plan {
	private double planCost;
	
	@Override
	public double calPlanCost(int noOfPeople,boolean isAutoPay,int age) {
			if(isAutoPay) {
				if(noOfPeople == 1) {
					planCost = 70;
				}else if(noOfPeople == 2) {
					planCost = noOfPeople*57; 
				}else if(noOfPeople == 3) {
					planCost = noOfPeople*45;
				}else if(noOfPeople == 4) {
					planCost = noOfPeople *40 ;
				}else if(noOfPeople > 4) {
					planCost = (noOfPeople *40) + ((noOfPeople-4)* 20);
				}
			}
			else {
				if(noOfPeople == 1) {
					planCost = 75;
				}else if(noOfPeople == 2) {
					planCost = noOfPeople*57 +(noOfPeople *5); 
				}else if(noOfPeople == 3) {
					planCost = noOfPeople*45  +(noOfPeople *5);
				}else if(noOfPeople == 4) {
					planCost = noOfPeople *40 +(noOfPeople *5) ;
				}else if(noOfPeople > 4) {
					planCost = (noOfPeople *40) + ((noOfPeople-4)* 20) +(noOfPeople *5);
				}
			}
		return planCost;
	}
}
