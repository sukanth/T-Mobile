package capgemini;

public class TmobileFamilyOfTwoPlan extends Plan{
	
	private double planCost;
	
	public double calPlanCost(int noOfPeople,boolean isAutoPay,int age) {
		if(isAutoPay) {
			planCost = noOfPeople *60;
		}
		else {
			planCost = (noOfPeople*60)+(noOfPeople*5);
		}
		return planCost;
	}
}
