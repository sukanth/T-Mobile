package capgemini;

public class TmobileSeniorCitizenPlan extends Plan {
	private double planCost;

	public double calPlanCost(int noOfPeople, boolean isAutoPay, int age) {
		if (isAutoPay && age> 55) {
			planCost = noOfPeople * 30;
		} else {
			planCost = (noOfPeople * 30) + (noOfPeople * 5);
		}
		return planCost;
	}

}
