package capgemini;
/**
 * 
 * @author sugunda
 *
 */
public abstract class Plan {
	
	int noOfPeople;
	double planCost;
	double dataLimit;
	 boolean autoPay;
	
	
	public abstract double calPlanCost(int noOfPeople,boolean isAutoPay,int age);

}
