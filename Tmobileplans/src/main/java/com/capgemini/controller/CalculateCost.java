package com.capgemini.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.bean.Details;

import capgemini.Plan;
import capgemini.TmobileFamilyOfTwoPlan;
import capgemini.TmobileOnePlan;
import capgemini.TmobileSeniorCitizenPlan;

/**
 * Servlet implementation class CalculateCostServlet
 */
public class CalculateCost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CalculateCost() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Plan plan;
		Details details = new Details();
		details.setAutoPay(request.getParameter("autoPay") != null);
		details.setFirstName(request.getParameter("firstName"));
		details.setLastName(request.getParameter("lastName"));
		details.setNoOfLines(Integer.parseInt(request.getParameter("lines")));
		details.setPhoneRequired(request.getParameter("phone") != null);
		details.setSocialSecurity(Long.parseLong(request.getParameter("socialSecurity")));
		details.setAge(Integer.parseInt(request.getParameter("age")));
		if ("onePlan".equalsIgnoreCase(request.getParameter("planName"))) {
			plan = new TmobileOnePlan();
			details.setTotalPlanCost(plan.calPlanCost(details.getNoOfLines(),details.isAutoPay(),details.getAge()));
		}else if("familyOfTwoPlan".equalsIgnoreCase(request.getParameter("planName"))) {
			plan = new TmobileFamilyOfTwoPlan();
			details.setTotalPlanCost(plan.calPlanCost(details.getNoOfLines(),details.isAutoPay(),details.getAge()));
		}
		else if("seniorCitizenPlan".equalsIgnoreCase(request.getParameter("planName"))) {
			plan = new TmobileSeniorCitizenPlan();
			details.setTotalPlanCost(plan.calPlanCost(details.getNoOfLines(),details.isAutoPay(),details.getAge()));
		}
		request.setAttribute("totalPlanCost",details.getTotalPlanCost());
		request.setAttribute("planName", request.getParameter("planName"));
		request.setAttribute("detailsBean", details);
		request.getRequestDispatcher("PlanSummary.jsp").forward(request, response);
	}
}
