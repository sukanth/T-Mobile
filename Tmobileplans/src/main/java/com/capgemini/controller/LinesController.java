package com.capgemini.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LinesController
 */
public class LinesController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LinesController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if ("TO".equalsIgnoreCase(request.getParameter("planCategory"))) {
			request.getRequestDispatcher("/onePlan.jsp").forward(request, response);
		} else if ("TSC".equalsIgnoreCase(request.getParameter("planCategory"))) {
			request.getRequestDispatcher("/seniorCitizenPlan.jsp").forward(request, response);
		} else if ("TFF".equalsIgnoreCase(request.getParameter("planCategory"))) {
			request.getRequestDispatcher("/familyForTwoPlan.jsp").forward(request, response);
		}
		if ("Back".equalsIgnoreCase(request.getParameter("Back"))) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		if("Proceed".equalsIgnoreCase(request.getParameter("Proceed"))) {
			if("onePlan".equalsIgnoreCase(request.getParameter("onePlan"))) {
				request.setAttribute("planName", request.getParameter("onePlan"));
			}else if("familyOfTwoPlan".equalsIgnoreCase(request.getParameter("familyOfTwoPlan"))) {
				request.setAttribute("planName", request.getParameter("familyOfTwoPlan"));
			}
			else if("seniorCitizenPlan".equalsIgnoreCase(request.getParameter("seniorCitizenPlan"))) {
				request.setAttribute("planName", request.getParameter("seniorCitizenPlan"));
			}
			request.getRequestDispatcher("/Details.jsp").forward(request, response);
		}
	}
}
