package com.capgemini.bean;

public class Details {
	
	String firstName;
	String lastName;
	long socialSecurity;
	boolean isAutoPay;
	int noOfLines;
	boolean isPhoneRequired;
	double totalPlanCost;
	int age;
	
	
	public double getTotalPlanCost() {
		return totalPlanCost;
	}
	public void setTotalPlanCost(double totalPlanCost) {
		this.totalPlanCost = totalPlanCost;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getSocialSecurity() {
		return socialSecurity;
	}
	public void setSocialSecurity(long socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
	public boolean isAutoPay() {
		return isAutoPay;
	}
	public void setAutoPay(boolean isAutoPay) {
		this.isAutoPay = isAutoPay;
	}
	public int getNoOfLines() {
		return noOfLines;
	}
	public void setNoOfLines(int noOfLines) {
		this.noOfLines = noOfLines;
	}
	public boolean isPhoneRequired() {
		return isPhoneRequired;
	}
	public void setPhoneRequired(boolean isPhoneRequired) {
		this.isPhoneRequired = isPhoneRequired;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
